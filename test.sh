#!/bin/bash


docker run sparrow_cloud:unittest /bin/bash -c \
    'source tests/mock_configmap.sh && py.test tests && py.test access_control'



source tests/mock_configmap.sh && python -m unittest tests/test_rest_client.py
source tests/mock_configmap.sh && python -m unittest tests