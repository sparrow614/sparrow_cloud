from django.apps import AppConfig


class TableAPIService(AppConfig):
    name = 'sparrow_cloud.apps.table_api'
    verbose_name = 'table_api'
