from django.apps import AppConfig


class SchemaCommandConfig(AppConfig):
    name = 'sparrow_cloud.apps.schema_command'
