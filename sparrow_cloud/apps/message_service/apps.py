from django.apps import AppConfig


class MessageService(AppConfig):
    name = 'sparrow_cloud.apps.message_service'
