from django.apps import AppConfig


class PingConfig(AppConfig):
    name = 'sparrow_cloud.apps.ping'
