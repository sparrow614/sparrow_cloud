from django.apps import AppConfig


class PermissionCommandConfig(AppConfig):
    name = 'sparrow_cloud.apps.permission_command'
